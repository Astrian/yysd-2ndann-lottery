const prizelist = require('./prizelist.json')
const fs = require('fs')
const random = require('seedrandom')
const readline = require("readline")
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

async function main () {
  let userlist
  try { userlist = fs.readFileSync('./userlist.txt', 'utf8') }
  catch(e) { return console.log('读取报名表单时出错：' + e) }
  userlist = userlist.split('\n')
  if (!prizelist) return console.log('读取奖品列表失败。')
  console.log('抽奖信息及名单已录入')
  console.log(`共有 ${userlist.length} 人参与抽奖\n`)
  console.log(`奖品列表：`)
  for (let i in prizelist) console.log(`👉${prizelist[i].name}（共 ${prizelist[i].num} 名）\n${prizelist[i].content}`)

  for (let i in prizelist) {
    prizelist[i].seeds = []
    for (let j = 0; j < prizelist[i].num; j++) prizelist[i].seeds[j] = await question(`输入 ${prizelist[i].name} 的第 ${j+1} 位中奖者随机数种子：`)
    prizelist[i].winner = []
    for (let j in prizelist[i].seeds) {
      let pointer = parseInt( random(prizelist[i].seeds[j])() * userlist.length )
      prizelist[i].winner[j] = userlist[pointer]
      userlist.splice(pointer, 1)
    }
    console.log('\n')
    console.log(`恭喜这${prizelist[i].num === 1 ? '位' : '些'}用户获得 ${prizelist[i].name}：`)
    for (let j in prizelist[i].winner) console.log(prizelist[i].winner[j])
    console.log('\n')
  }
}

async function question (q) {
  return new Promise((res) => {
    rl.question(q, r => {
      res(r)
    })
  })
}

main()